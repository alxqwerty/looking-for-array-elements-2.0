﻿using System;

namespace LookingForArrayElementsRecursion
{
    public static class FloatCounter
    {
        public static int GetFloatsCount(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd)
        {
            int counter = 0;
            int inSearchIndex = 0;
            int forSearchIndex = 0;

            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), "Array is null.");
            }

            if (rangeStart is null)
            {
                throw new ArgumentNullException(nameof(rangeStart), "Array is null.");
            }

            if (rangeEnd is null)
            {
                throw new ArgumentNullException(nameof(rangeEnd), "Array is null.");
            }

            if (rangeStart.Length != rangeEnd.Length)
            {
                throw new ArgumentException("Parameters value aren't equal.", nameof(rangeStart));
            }

            if (rangeStart.Length == 0 && rangeEnd.Length == 0)
            {
                return 0;
            }

            if (rangeStart[forSearchIndex] > rangeEnd[forSearchIndex])
            {
                throw new ArgumentException("Invalid value.", nameof(rangeStart));
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, inSearchIndex, forSearchIndex);
        }

        public static int GetFloatsCount(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), "Array is null.");
            }

            if (rangeStart is null)
            {
                throw new ArgumentNullException(nameof(rangeStart), "Array is null.");
            }

            if (rangeEnd is null)
            {
                throw new ArgumentNullException(nameof(rangeEnd), "Array is null.");
            }

            if (rangeStart.Length != rangeEnd.Length)
            {
                throw new ArgumentException("Parameters value aren't equal.", nameof(rangeStart));
            }

            if (startIndex < 0 || startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Value is out of range.");
            }

            if (count < 0)
            {
                throw new ArgumentException("Invalid value.", nameof(count));
            }

            if (rangeStart.Length == 0 && rangeEnd.Length == 0)
            {
                return 0;
            }

            int counter = 0;
            int forSearchIndex = 0;
            int length = startIndex + count;

            return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, startIndex, forSearchIndex, length);
        }

        public static int NextStep(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd, int counter, int inSearchIndex, int forSearchIndex)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), "Array is null.");
            }

            if (rangeStart is null)
            {
                throw new ArgumentNullException(nameof(rangeStart), "Array is null.");
            }

            if (rangeEnd is null)
            {
                throw new ArgumentNullException(nameof(rangeEnd), "Array is null.");
            }

            if (inSearchIndex < 0 || inSearchIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(inSearchIndex), "Value is out of range.");
            }

            if (forSearchIndex < 0 || forSearchIndex > rangeStart.Length || forSearchIndex > rangeEnd.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(forSearchIndex), "Value is out of range.");
            }

            if (inSearchIndex < arrayToSearch.Length)
            {
                if (arrayToSearch[inSearchIndex] >= rangeStart[forSearchIndex] && arrayToSearch[inSearchIndex] <= rangeEnd[forSearchIndex]
                    && forSearchIndex == rangeStart.Length - 1)
                {
                    counter++;
                    inSearchIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, inSearchIndex, forSearchIndex);
                }
                else if (arrayToSearch[inSearchIndex] >= rangeStart[forSearchIndex] && arrayToSearch[inSearchIndex] <= rangeEnd[forSearchIndex]
                    && forSearchIndex < rangeStart.Length - 1)
                {
                    counter++;
                    forSearchIndex++;
                    return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, inSearchIndex, forSearchIndex);
                }
                else if (forSearchIndex < rangeStart.Length - 1)
                {
                    forSearchIndex++;
                    return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, inSearchIndex, forSearchIndex);
                }
                else
                {
                    inSearchIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, inSearchIndex, forSearchIndex);
                }
            }
            else
            {
                return counter;
            }
        }

        public static int NextStep(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd, int counter, int startIndex, int forSearchIndex, int limit)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), "Array is null.");
            }

            if (rangeStart is null)
            {
                throw new ArgumentNullException(nameof(rangeStart), "Array is null.");
            }

            if (rangeEnd is null)
            {
                throw new ArgumentNullException(nameof(rangeEnd), "Array is null.");
            }

            if (startIndex < 0 || startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Value is out of range.");
            }

            if (forSearchIndex < 0 || forSearchIndex > rangeStart.Length || forSearchIndex > rangeEnd.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(forSearchIndex), "Value is out of range.");
            }

            if (limit < 0 || limit > arrayToSearch.Length)
            {
                throw new ArgumentException("Value is out of range.", nameof(limit));
            }

            if (startIndex < limit)
            {
                if (rangeStart[forSearchIndex] > rangeEnd[forSearchIndex])
                {
                    throw new ArgumentException("Invalid value.", nameof(rangeStart));
                }

                if (arrayToSearch[startIndex] >= rangeStart[forSearchIndex] && arrayToSearch[startIndex] <= rangeEnd[forSearchIndex]
                    && forSearchIndex == rangeStart.Length - 1)
                {
                    counter++;
                    startIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, startIndex, forSearchIndex, limit);
                }
                else if (arrayToSearch[startIndex] >= rangeStart[forSearchIndex] && arrayToSearch[startIndex] <= rangeEnd[forSearchIndex]
                    && forSearchIndex < rangeStart.Length - 1)
                {
                    counter++;
                    forSearchIndex++;
                    return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, startIndex, forSearchIndex, limit);
                }
                else if (forSearchIndex < rangeStart.Length - 1)
                {
                    forSearchIndex++;
                    return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, startIndex, forSearchIndex, limit);
                }
                else
                {
                    startIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, rangeStart, rangeEnd, counter, startIndex, forSearchIndex, limit);
                }
            }
            else
            {
                return counter;
            }
        }
    }
}
