﻿using System;

namespace LookingForArrayElementsRecursion
{
    public static class IntegersCounter
    {
        public static int GetIntegersCount(int[] arrayToSearch, int[] elementsToSearchFor)
        {
            int counter = 0;
            int inSearchIndex = 0;
            int forSearchIndex = 0;

            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), "Array is null.");
            }

            if (elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(elementsToSearchFor), "Array is null.");
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            if (elementsToSearchFor.Length == 0)
            {
                return 0;
            }

            return NextStep(arrayToSearch, elementsToSearchFor, counter, inSearchIndex, forSearchIndex);
        }

        public static int GetIntegersCount(int[] arrayToSearch, int[] elementsToSearchFor, int startIndex, int count)
        {
            int counter = 0;
            int forSearchIndex = 0;
            int length = startIndex + count;

            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), "Array is null.");
            }

            if (elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(elementsToSearchFor), "Array is null.");
            }

            if (startIndex < 0 || startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Value is out of range.");
            }

            if (count < 0)
            {
                throw new ArgumentException("Invalid value.", nameof(count));
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            if (elementsToSearchFor.Length == 0)
            {
                return 0;
            }

            return NextStep(arrayToSearch, elementsToSearchFor, counter, startIndex, forSearchIndex, length);
        }

        public static int NextStep(int[] arrayToSearch, int[] elementsToSearchFor, int counter, int inSearchIndex, int forSearchIndex)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), "Array is null.");
            }

            if (elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(elementsToSearchFor), "Array is null.");
            }

            if (inSearchIndex < 0 || inSearchIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(inSearchIndex), "Value is out of range.");
            }

            if (forSearchIndex < 0 || forSearchIndex > elementsToSearchFor.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(forSearchIndex), "Value is out of range.");
            }

            if (inSearchIndex < arrayToSearch.Length)
            {
                if (arrayToSearch[inSearchIndex] == elementsToSearchFor[forSearchIndex])
                {
                    counter++;
                    inSearchIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, elementsToSearchFor, counter, inSearchIndex, forSearchIndex);
                }
                else if (arrayToSearch[inSearchIndex] != elementsToSearchFor[forSearchIndex] && forSearchIndex < elementsToSearchFor.Length - 1)
                {
                    forSearchIndex++;
                    return NextStep(arrayToSearch, elementsToSearchFor, counter, inSearchIndex, forSearchIndex);
                }
                else
                {
                    inSearchIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, elementsToSearchFor, counter, inSearchIndex, forSearchIndex);
                }
            }
            else
            {
                return counter;
            }
        }

        public static int NextStep(int[] arrayToSearch, int[] elementsToSearchFor, int counter, int startIndex, int forSearchIndex, int limit)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), "Array is null.");
            }

            if (elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(elementsToSearchFor), "Array is null.");
            }

            if (startIndex < 0 || startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Value is out of range.");
            }

            if (forSearchIndex < 0 || forSearchIndex > elementsToSearchFor.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(forSearchIndex), "Value is out of range.");
            }

            if (limit < 0 || limit > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(limit), "Value is out of range.");
            }

            if (startIndex < limit)
            {
                if (arrayToSearch[startIndex] == elementsToSearchFor[forSearchIndex])
                {
                    counter++;
                    startIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, elementsToSearchFor, counter, startIndex, forSearchIndex, limit);
                }
                else if (arrayToSearch[startIndex] != elementsToSearchFor[forSearchIndex] && forSearchIndex < elementsToSearchFor.Length - 1)
                {
                    forSearchIndex++;
                    return NextStep(arrayToSearch, elementsToSearchFor, counter, startIndex, forSearchIndex, limit);
                }
                else
                {
                    startIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, elementsToSearchFor, counter, startIndex, forSearchIndex, limit);
                }
            }
            else
            {
                return counter;
            }
        }
    }
}
