﻿using System;

#pragma warning disable S2368

namespace LookingForArrayElementsRecursion
{
    public static class DecimalCounter
    {
        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            int counter = 0;
            int inSearchIndex = 0;
            int forSearchIndex = 0;

            CheckRanges(ranges, forSearchIndex);

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            if (ranges.Length == 0)
            {
                return 0;
            }

            return NextStep(arrayToSearch, ranges, counter, inSearchIndex, forSearchIndex);
        }

        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length == 0)
            {
                return 0;
            }

            int counter = 0;
            int forSearchIndex = 0;
            int length = startIndex + count;

            CheckRanges(ranges, forSearchIndex);

            if (count < 0 || count > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            return NextStep(arrayToSearch, ranges, counter, startIndex, forSearchIndex, length);
        }

        public static int NextStep(decimal[] arrayToSearch, decimal[][] ranges, int counter, int inSearchIndex, int forSearchIndex)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (inSearchIndex < arrayToSearch.Length)
            {
                if (arrayToSearch[inSearchIndex] >= ranges[forSearchIndex][0] && arrayToSearch[inSearchIndex] <= ranges[forSearchIndex][1])
                {
                    counter++;
                    inSearchIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, ranges, counter, inSearchIndex, forSearchIndex);
                }
                else if (forSearchIndex < ranges.Length - 1 && ranges[forSearchIndex + 1].Length != 0)
                {
                    forSearchIndex++;
                    return NextStep(arrayToSearch, ranges, counter, inSearchIndex, forSearchIndex);
                }
                else
                {
                    inSearchIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, ranges, counter, inSearchIndex, forSearchIndex);
                }
            }
            else
            {
                return counter;
            }
        }

        public static int NextStep(decimal[] arrayToSearch, decimal[][] ranges, int counter, int startIndex, int forSearchIndex, int limit)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (startIndex < limit)
            {
                if (arrayToSearch[startIndex] >= ranges[forSearchIndex][0] && arrayToSearch[startIndex] <= ranges[forSearchIndex][1])
                {
                    counter++;
                    startIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, ranges, counter, startIndex, forSearchIndex, limit);
                }
                else if (forSearchIndex < ranges.Length - 1 && ranges[forSearchIndex + 1].Length != 0)
                {
                    forSearchIndex++;
                    return NextStep(arrayToSearch, ranges, counter, startIndex, forSearchIndex, limit);
                }
                else
                {
                    startIndex++;
                    forSearchIndex = 0;
                    return NextStep(arrayToSearch, ranges, counter, startIndex, forSearchIndex, limit);
                }
            }
            else
            {
                return counter;
            }
        }

        public static int CheckRanges(decimal[][] ranges, int forSearchIndex)
        {
            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length == 0)
            {
                return 0;
            }

            if (ranges[forSearchIndex] is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges[forSearchIndex].Length == 0)
            {
                return 0;
            }

            if (ranges[forSearchIndex].Length != 2)
            {
                throw new ArgumentException("Length cannot be less or bigger than 2.", nameof(ranges));
            }

            if (forSearchIndex < ranges.Length - 1)
            {
                forSearchIndex++;
                return CheckRanges(ranges, forSearchIndex);
            }

            if (forSearchIndex == ranges.Length - 1 && ranges[forSearchIndex].Length == 2)
            {
                return 1;
            }

            return CheckRanges(ranges, forSearchIndex);
        }
    }
}
